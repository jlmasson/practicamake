CC=gcc
CFLAGS=-lm -I.
DEPS = point.h
OBJ = main.o point.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

point: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

clean:
	rm -f point *.o
