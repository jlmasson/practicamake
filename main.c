#include <stdio.h>
#include <point.h>
#define PUNTOS 2
Point ingresarDatos();

int main(){
	
	struct Point puntos[PUNTOS];
	
	printf("Distancia euclidiana entre dos puntos (3D)\n");
	
	for (int i = 0; i < PUNTOS; i++) {
		printf("Punto %d:\n", i + 1);		
		Point p;
		p = ingresarDatos();
		puntos[i] = p;
	}	

	float d;
	d = calcularDistancia(puntos[0], puntos[1]);
	printf("La distancia euclidiana entre los dos puntos es: %.2f unidades\n", d);


}

Point ingresarDatos(){
	
	Point p;	
	
	printf("Ingresar coordenada x: ");
	scanf("%f", &p.x);
	
	printf("Ingresar coordenada y: ");
	scanf("%f", &p.y);

	printf("Ingresar coordenada z: ");
	scanf("%f", &p.z);

	printf("\n");

	return p;

}


