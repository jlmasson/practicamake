#include <point.h>
#include <math.h>

float calcularDistancia(Point p1, Point p2) {
	float expresion = powf(p1.x - p2.x, 2) + powf(p1.y - p2.y, 2) + powf(p1.z - p2.z, 2);

	float distancia = sqrt(expresion);

	return distancia;
}
